const express = require('express')
const router = express.Router()
const userController = require('../controllers/userControllers')

//Routes for checking if ther user's email already exists in the database
router.post('/checkEmail', (req,res)=>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
})

router.post('/register', (req,res)=>{
	userController.registerUser(req.body).then(result => res.send(result))
})

//Route for user authentication
router.post('/login', (req,res)=>{
	userController.loginUser(req.body).then(result => res.send(result))
})

//s33
router.get('/details', (req,res)=>{
	userController.getProfile(req.body).then(result => res.send(result))
})

module.exports = router